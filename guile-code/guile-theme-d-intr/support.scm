
;; Copyright (C) 2016, 2018, 2022, 2024 Tommi Höynälänmaa

;; This file is part of Theme-D-Golf.

;; You can redistribute and/or modify this file under the terms of the
;; GNU Lesser General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any
;; later version.

;; This file is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public
;; License along with this program. If not, see
;; https://www.gnu.org/licenses/.


(define-module (guile-theme-d-intr support))
         
(export theme-gtype-instance-signal-connect slot-set0!)
         
(import (srfi srfi-1)
        (oop goops)
        (except (g-golf) map)
        (theme-d runtime rte-base)
        (theme-d runtime runtime-theme-d-environment)
        (th-scheme-utilities stdutils))

(define (theme-gtype-instance-signal-connect obj sym-name proc-handler)
  (connect
   obj sym-name
   (lambda args
     (_i_call-proc proc-handler
                   args
                   (map theme-type-of args))))
  #f)

(define slot-set0! slot-set!)


