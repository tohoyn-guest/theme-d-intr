
;; Copyright (C) 2016, 2018, 2021, 2022, 2024 Tommi Höynälänmaa

;; This file is part of Theme-D-Golf.

;; You can redistribute and/or modify this file under the terms of the
;; GNU Lesser General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any
;; later version.

;; This file is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public
;; License along with this program. If not, see
;; https://www.gnu.org/licenses/.


(define-module (guile-theme-d-intr support2-gtk4))

(export gtk-tree-selection-get-selected-iter
        gtk-tree-model-get-iter1
        gtk-message-dialog-new1)
;; gdk-event-get-changed-mask
;; gdk-event-get-new-window-state)

(import (guile)
        (ice-9 receive)
        (srfi srfi-1)
        (oop goops)
        (theme-d runtime runtime-theme-d-environment)
        (th-scheme-utilities stdutils))

(eval-when (expand load eval)
           (import (g-golf))
           (define gl-str-gtk-version "4.0")
           (gi-import-by-name "Gtk" "TreeModel"
                              #:version gl-str-gtk-version)
           (gi-import-by-name "Gtk" "TreeSelection"
                              #:version gl-str-gtk-version)
           (gi-import-by-name "Gtk" "MessageDialog"
                              #:version gl-str-gtk-version))

(define (gtk-tree-selection-get-selected-iter tree-selection)
  (receive (model iter)
           (gtk-tree-selection-get-selected tree-selection)
           iter))

(define (gtk-tree-model-get-iter1 treemodel treepath)
  (receive (valid? iter)
           (gtk-tree-model-get-iter treemodel treepath)
           iter))

(define (gtk-message-dialog-new1 buttons str-text)
  (let ((dialog
         (make <gtk-message-dialog> #:buttons buttons)))
    (set! (!modal dialog) #t)
    (set! (!destroy-with-parent dialog) #t)
    (set! (!text dialog) str-text)
    dialog))

;; (define (gdk-event-get-changed-mask event)
;;   (!changed-mask event))

;; (define (gdk-event-get-new-window-state event)
;;   (!new-window-state event)))
