
;; Copyright (C) 2020, 2022-2024 Tommi Höynälänmaa

;; This file is part of Theme-D-Intr.

;; You can redistribute and/or modify this file under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any
;; later version.

;; This file is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see https://www.gnu.org/licenses/.


(intr-entities
 (version Gtk "3.0")
 (classes
  (Gtk Widget)
  (Gtk Application)
  (Gtk ApplicationWindow)
  (Gtk Window)
  (Gtk Button)
  (Gtk CheckButton)
  (Gtk Dialog)
  (Gtk MessageDialog)
  (Gtk ButtonsType)
  (Gtk VBox)
  (Gtk HBox)
  (Gtk Paned)
  (Gtk VPaned)
  (Gtk HPaned)
  (Gtk ScrolledWindow)
  (Gtk TextBuffer)
  (Gtk TextView)
  (Gtk Entry)
  (Gtk Label)
  (Gtk HSeparator)
  (Gtk TextIter)
  (Gtk TextTagTable)
  (Gtk TextTag)
  (Gtk TextMark)
  (Gtk TreeView)
  (Gtk TreePath)
  (Gtk TreeViewColumn)
  (Gtk TreeSelection)
  (Gtk ListStore)
  (Gtk TreeStore)
  (Gtk CellRendererText)
  (Gtk CellRendererToggle)
  (Gtk TreeModel)
  (Gtk Frame)
  (Gtk Table)
  (Gtk Notebook)
  (Gtk Image)
  (Gtk SizeGroup)
  (Gtk CheckButton)
  (Gtk ComboBoxText)
  (Gtk ButtonBox)
  (Gtk HButtonBox)
  (Gtk VButtonBox)
  (Gtk Action)
  (Gtk Statusbar)
  (Gtk MenuBar)
  (Gtk Menu)
  (Gtk MenuItem)
  (Gtk CheckMenuItem)
  (Gtk RadioMenuItem)
  (Gtk SeparatorMenuItem)
  (Gtk Toolbar)
  (Gtk ToolItem)
  (Gtk ToolButton)
  (Gtk SeparatorToolItem)
  (Gdk Event))
 (rejected-methods
  append map get-style activate compare copy free get-flags delete)
 (overridden-functions
  (gtk-container-child-get-property (<gtk-container> <gtk-widget> <string>)
                                    <object> nonpure))
 (ignore-slots (Gtk Label) xalign yalign))
