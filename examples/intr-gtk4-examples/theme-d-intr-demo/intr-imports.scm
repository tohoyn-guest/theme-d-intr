
;; Copyright (C) 2020, 2021, 2024 Tommi Höynälänmaa

;; This file is part of Theme-D-Golf.

;; You can redistribute and/or modify this file under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any
;; later version.

;; This file is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see https://www.gnu.org/licenses/.


(intr-entities
 (version Gtk "4.0")
 (classes
  (GObject Object)
  (Gdk Display)
  (Gdk Clipboard)
  (Gtk Widget)
  (Gtk CssProvider)
  (Gtk StyleContext)
  (Gtk Application)
  (Gtk ApplicationWindow)
  (Gtk Window)
  (Gtk Button)
  (Gtk CheckButton)
  (Gtk Dialog)
  (Gtk MessageDialog)
  (Gtk ButtonsType)
  (Gtk Box)
  (Gtk Paned)
  (Gtk ScrolledWindow)
  (Gtk TextBuffer)
  (Gtk TextView)
  (Gtk Entry)
  (Gtk EntryBuffer)
  (Gtk Label)
  (Gtk Separator)
  (Gtk TextIter)
  (Gtk TextTagTable)
  (Gtk TextTag)
  (Gtk TextMark)
  (Gtk TreeView)
  (Gtk TreePath)
  (Gtk TreeViewColumn)
  (Gtk TreeSelection)
  (Gtk ListStore)
  (Gtk TreeStore)
  (Gtk CellRendererText)
  (Gtk CellRendererToggle)
  (Gtk TreeModel)
  (Gtk Frame)
  (Gtk Grid)
  (Gtk Notebook)
  (Gtk Image)
  (Gtk SizeGroup)
  (Gtk CheckButton)
  (Gtk ComboBoxText)
  (Gtk Statusbar)
  (Gdk Event))
 (rejected-methods
  append map get-style activate compare copy get-cursor free
  get-name get-state apply get-flags delete))
