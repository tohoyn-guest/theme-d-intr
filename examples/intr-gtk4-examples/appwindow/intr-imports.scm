
;; Copyright (C) 2020, 2022-2024 Tommi Höynälänmaa

;; This file is part of Theme-D-Intr.

;; You can redistribute and/or modify this file under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any
;; later version.

;; This file is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see https://www.gnu.org/licenses/.


(intr-entities
 (version Gtk "4.0")
 (classes
  (GObject Object)
  (Gtk Widget)
  (Gtk Application)
  (Gtk ApplicationWindow)
  (Gtk Builder)
  (Gtk Application)
  (Gtk ApplicationWindow)
  (Gtk Statusbar)
  (Gtk ScrolledWindow)
  (Gtk TextView)
  (Gtk TextBuffer)
  (Gtk TextIter)
  (Gtk Box)
  (Gtk Button)
  (Gtk Label)
  (Gtk Window)
  (Gio Action)
  (Gio SimpleAction)
  (Gio ActionMap)
  (Gio Settings)
  (Gio SettingsSchemaSource)
  (Adw MessageDialog))
 (rejected-methods
  append map get-style activate compare copy get-cursor free
  get-name get-state apply))
