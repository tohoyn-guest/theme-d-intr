
# Copyright (C) 2020-2022 Tommi Höynälänmaa

# This file is part of Theme-D-Intr.

# You can redistribute and/or modify this file under the terms of the
# GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any
# later version.

# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see https://www.gnu.org/licenses/.


.PHONY: all clean

ifdef THEME_D_LIB_DIR
  MODULE_OPTION = -m ../..:$(THEME_D_LIB_DIR)
else
  MODULE_OPTION = -m ../..:
endif

GEN_NAME := "(intr-gtk4-examples calc _intr-imports)"
INTR_MODULE := "(intr-gtk4-examples calc _intr-imports-target)"

EXTRA_COMP_OPTIONS ?=
EXTRA_LINK_OPTIONS ?=

TDC := theme-d-compile

TDL := theme-d-link

GEN_INTF := generate-intr-interface
GEN_BODY := generate-intr-body
GEN_TM := generate-intr-target-module

all: main.go

clean:
	-rm main.go main.tcp main.thp _intr-imports.tci _intr-imports.tcb \
	  _intr-imports.thi _intr-imports.thb _intr-imports-target.scm

main.thp: main.thp.in
	sed -e "s|@programdir@|$$(pwd)|g" $< > $@

main.go: main.tcp _intr-imports.tci _intr-imports.tcb _intr-imports-target.scm
	$(TDL) $(MODULE_OPTION) \
	  -x "(g-golf)" \
	  -x "(guile-theme-d-intr support)" \
	  -x $(INTR_MODULE) \
	  --duplicates="merge-generics replace warn-override-core warn last" \
	  --extra-guild-options="@extra_guild_options@" \
	  $(EXTRA_LINK_OPTIONS) $<

main.tcp: _intr-imports.tci

_intr-imports.thi: intr-imports.scm
	$(GEN_INTF) -m $(GEN_NAME)

_intr-imports.thb: intr-imports.scm
	$(GEN_BODY) -m $(GEN_NAME)

_intr-imports-target.scm: intr-imports.scm
	$(GEN_TM) -m $(INTR_MODULE)

%.tci: %.thi
	$(TDC) $(MODULE_OPTION) $(EXTRA_COMP_OPTIONS) $<

%.tcb: %.thb
	$(TDC) $(MODULE_OPTION) $(EXTRA_COMP_OPTIONS) $<

%.tcp: %.thp
	$(TDC) $(MODULE_OPTION) $(EXTRA_COMP_OPTIONS) $<
