
;; Copyright (C) 2025 Tommi Höynälänmaa

;; This file is part of Theme-D-Intr.

;; You can redistribute and/or modify this file under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any
;; later version.

;; This file is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see https://www.gnu.org/licenses/.


(intr-entities
 (version Gtk "4.0")
 (classes
  (Gtk Widget)
  (Gtk Application)
  (Gtk ApplicationWindow)
  (Gtk Button)
  (Gtk Box)
  (Gtk TextView)
  (Gtk TextBuffer)
  (Gtk TextTag)
  (Gtk TextTagTable)))

