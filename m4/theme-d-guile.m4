AC_DEFUN([CHECK_GUILE_VERSION],[
PKG_CHECK_EXISTS([guile-3.0], [HAVE_GUILE_3_0=yes], [HAVE_GUILE_3_0=no])
PKG_CHECK_EXISTS([guile-2.2], [HAVE_GUILE_2_2=yes], [HAVE_GUILE_2_2=no])
PKG_CHECK_EXISTS([guile-2.0], [HAVE_GUILE_2_0=yes], [HAVE_GUILE_2_0=no])
if test "$guile_spec" = no ; then
  if test $HAVE_GUILE_3_0 = yes ; then
    MY_GUILE_VERSION=3.0 ;
  elif test $HAVE_GUILE_2_2 = yes ; then
    MY_GUILE_VERSION=2.2 ;
  elif test $HAVE_GUILE_2_0 = yes ; then
    MY_GUILE_VERSION=2.0 ;
  else
    echo "Error: Guile version 2.0, 2.2, or 3.0 not found." ;
    exit 1;
  fi
elif test "$guile_spec" = 3.0 ; then
  if test $HAVE_GUILE_3_0 = yes ; then
    MY_GUILE_VERSION=3.0 ;
  else
    echo "Error: Guile version 3.0 not found." ;
    exit 1;
  fi
elif test "$guile_spec" = 2.2 ; then
  if test $HAVE_GUILE_2_2 = yes ; then
    MY_GUILE_VERSION=2.2 ;
  else
    echo "Error: Guile version 2.2 not found." ;
    exit 1;
  fi
elif test "$guile_spec" = 2.0 ; then
  if test $HAVE_GUILE_2_0 = yes ; then
    MY_GUILE_VERSION=2.0 ;
  else
    echo "Error: Guile version 2.0 not found." ;
    exit 1;
  fi
else
  echo "Error: Invalid Guile version." ;
  exit 1;
fi
])
